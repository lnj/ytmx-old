# SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import json

from BrowseEndpoint import BrowseEndpoint


class Thumbnail:
    width: int
    height: int
    url: str

    @staticmethod
    def fromThumbnailsJson(thumbnailsJson):
        thumbnails = []

        for thumb in thumbnailsJson["musicThumbnailRenderer"]["thumbnail"]["thumbnails"]:
            thumbnails.append(Thumbnail(thumb["width"], thumb["height"], thumb["url"]))

        return thumbnails

    def __init__(self, width, height, url):
        self.width = width
        self.height = height
        self.url = url


class SongItem:
    thumbnails: list
    title: str
    artist: str
    album: str
    videoId: str

    def __init__(self, listItemRenderer):
        self.thumbnails = Thumbnail.fromThumbnailsJson(listItemRenderer["thumbnail"])

        for flexColumn in listItemRenderer["flexColumns"]:
            runs = flexColumn["musicResponsiveListItemFlexColumnRenderer"]["text"]["runs"][0]
            if "navigationEndpoint" in runs:
                navEndpoint = runs["navigationEndpoint"]

                """
                Song title
                """
                if "watchEndpoint" in navEndpoint:
                    watchEndpoint = navEndpoint["watchEndpoint"]

                    self.title = runs["text"]
                    self.videoId = watchEndpoint["videoId"]

                """
                Album / Artist
                """
                if "browseEndpoint" in navEndpoint:
                    browseEndpoint = navEndpoint["browseEndpoint"]
                    pageType = browseEndpoint["browseEndpointContextSupportedConfigs"]["browseEndpointContextMusicConfig"]["pageType"]

                    if pageType == "MUSIC_PAGE_TYPE_ARTIST":
                        self.artist = runs["text"]
                    elif pageType == "MUSIC_PAGE_TYPE_ALBUM":
                        self.album = runs["text"]

    def __str__(self):
        return "[{}] {}:\t {}".format(self.artist, self.album, self.title)

    def __lt__(self, other):
        return other.album < self.album


class MusicPlaylistShelf:
    songs = []
    continuation = str()

    def __init__(self, musicPlaylistShelf):
        items = musicPlaylistShelf["contents"]

        for item in items:
            self.songs.append(
                SongItem(
                    item["musicResponsiveListItemRenderer"]
                )
            )

        self.songs.sort()

        if "continuations" in musicPlaylistShelf:
            self.continuation = musicPlaylistShelf["continuations"][0]["nextContinuationData"]["continuation"]


class ChannelHeader:
    title = str()
    description = str()
    thumbnails = []

    def __init__(self, musicHeader):
        self.thumbnails = Thumbnail.fromThumbnailsJson(musicHeader["thumbnail"])
        self.title = musicHeader["title"]["runs"][0]["text"]
        self.description = musicHeader["description"]["runs"][0]["text"]


class BrowseReply:
    continuation = str()
    playlists = []
    header: ChannelHeader
    allSongsEndpoint: BrowseEndpoint
    allAlbumsEndpoint: BrowseEndpoint
    allSinglesEndpoint: BrowseEndpoint
    allVideosEndpoint: BrowseEndpoint

    def __init__(self, input):
        jsonInput = json.loads(input)

        """
        header
        """
        if "header" in jsonInput:
            header = jsonInput["header"]

            if "musicImmersiveHeaderRenderer" in header:
                self.header = ChannelHeader(header["musicImmersiveHeaderRenderer"])

        """
        contents
        """
        if "contents" in jsonInput:
            print(jsonInput["contents"]["singleColumnBrowseResultsRenderer"])
            
            sectionListContents = jsonInput["contents"]["singleColumnBrowseResultsRenderer"]["tabs"][0]["tabRenderer"]["content"]["sectionListRenderer"]["contents"]
            
            for section in sectionListContents:
                if "musicPlaylistShelfRenderer" in section:
                    self.playlists.append(
                        MusicPlaylistShelf(section["musicPlaylistShelfRenderer"])
                    )

                elif "musicShelfRenderer" in section:
                    # this contains a list of some of the most popular songs
                    # we only need the browse endpoint to the full list
                    self.allSongsEndpoint = BrowseEndpoint.fromJson(
                        section["musicShelfRenderer"]["bottomEndpoint"]["browseEndpoint"]
                    )

                elif "musicCarouselShelfRenderer" in section:
                    header = section["musicCarouselShelfRenderer"]["header"]["musicCarouselShelfBasicHeaderRenderer"]
                    titleMeta = header["title"]["runs"][0]
                    title = titleMeta["text"]
                    titleEndpoint = None
                    
                    if "navigationEndpoint" in titleMeta:
                        titleEndpoint = BrowseEndpoint.fromJson(
                            titleMeta["navigationEndpoint"]["browseEndpoint"]
                        )

                    if titleEndpoint:
                        if title == "Albums":
                            self.allAlbumsEndpoint = titleEndpoint
                        elif title == "Singles":
                            self.allSinglesEndpoint = titleEndpoint
                        elif title == "Videos":
                            self.allVideosEndpoint = titleEndpoint

                elif "musicDescriptionShelfRenderer" in section:
                    pass

        elif "continuationContents" in jsonInput:
            if "musicPlaylistShelfContinuation" in jsonInput["continuationContents"]:
                self.playlists = [
                    MusicPlaylistShelf(
                        jsonInput["continuationContents"]["musicPlaylistShelfContinuation"]
                    )
                ]

        else:
            print("Couldn't parse browse reply!")
            return

    def __str__(self):
        output = str()
        for playlist in self.playlists:
            for song in playlist.songs:
                output += str(song) + "\n"
            output += "\n"
        return output
