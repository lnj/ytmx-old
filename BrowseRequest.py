# SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import json
import requests

from YTMConfig import YTMConfig

BROWSE_ENDPOINT = YTMConfig.YTMUSIC_URL + "/youtubei/v1/browse?alt=json&key={}"
BROWSE_CONTINUATION = "&continuation={}"

class BrowseRequest:
    browseId: str
    continuation: str
    params: str

    def __init__(self, browseId, continuation = str(), params = str()):
        self.browseId = browseId
        self.continuation = continuation
        self.params = params

    def __contextJson(self):
        return {
            "client": {
                "clientName": "WEB_REMIX",
                "clientVersion": "0.1",
                "hl": "en",
                "gl": "DE",
                "experimentIds": [],
                "experimentsToken": "",
                "browserName": "Firefox",
                "browserVersion": "80.0",
                "osName": "X11",
                "utcOffsetMinutes": 120,
                "locationInfo": {
                    "locationPermissionAuthorizationStatus": "LOCATION_PERMISSION_AUTHORIZATION_STATUS_UNSUPPORTED"
                },
                "musicAppInfo": {
                    "musicActivityMasterSwitch": "MUSIC_ACTIVITY_MASTER_SWITCH_INDETERMINATE",
                    "musicLocationMasterSwitch": "MUSIC_LOCATION_MASTER_SWITCH_INDETERMINATE",
                    "pwaInstallabilityStatus": "PWA_INSTALLABILITY_STATUS_UNKNOWN"
                }
            },
            "capabilities": {},
            "request": {
                "internalExperimentFlags": [
                    {
                        "key": "force_music_enable_outertube_search_suggestions",
                        "value": "true"
                    },
                    {
                        "key": "force_music_enable_outertube_music_queue",
                        "value": "true"
                    },
                    {
                        "key": "force_route_music_library_subscriptions_to_outertube",
                        "value": "true"
                    },
                    {
                        "key": "force_route_music_library_albums_to_outertube",
                        "value": "true"
                    },
                    {
                        "key": "force_music_enable_outertube_home_browse",
                        "value": "true"
                    },
                    {
                        "key": "force_route_music_library_track_artists_to_outertube",
                        "value": "true"
                    },
                    {
                        "key": "force_music_enable_outertube_album_detail_browse",
                        "value": "true"
                    },
                    {
                        "key": "force_music_enable_outertube_playlist_detail_browse",
                        "value": "true"
                    },
                    {
                        "key": "force_music_enable_outertube_tastebuilder_browse",
                        "value": "true"
                    }
                ],
                "sessionIndex": 0
            },
            "clickTracking": {
                "clickTrackingParams": "CK4EEPleGAAiEwip2buHmuzrAhU4iN4KHdELDcQ="
            },
            "activePlayers": {},
            "user": {
                "enableSafetyMode": False
            }
        }

    def send(self, apiKey, referer=str()):
        requestJson = {
            "context": self.__contextJson(),
            "browseId": self.browseId
        }

        if self.params:
            requestJson["params"] = self.params
            

        if self.browseId.startswith("UC"):
            """
            User Channel (UC) browse ids
            """
            requestJson["browseEndpointContextSupportedConfigs"] = {
                "browseEndpointContextMusicConfig": {
                    "pageType": "MUSIC_PAGE_TYPE_ARTIST"
                }
            }
        elif self.browseId.startswith("VLPL") and "params" not in requestJson:
            requestJson["params"] = "ggMCCAI%3D"

        url = BROWSE_ENDPOINT.format(apiKey)
        if self.continuation:
            url += BROWSE_CONTINUATION.format(self.continuation)

        return requests.post(
            url,
            json.dumps(requestJson),
            headers={
                "User-Agent": YTMConfig.USER_AGENT,
                "Referer": referer or YTMConfig.YTMUSIC_URL
            }
        )
