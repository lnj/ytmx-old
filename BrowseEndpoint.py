# SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

class BrowseEndpoint:
    id = str()
    params = str()

    @staticmethod
    def fromJson(json):
        endpoint = BrowseEndpoint(json["browseId"])

        if "params" in json:
            endpoint.params = json["params"]

        return endpoint

    def __init__(self, id, params = str()):
        self.id = id
        self.params = params
