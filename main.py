#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import requests
import time

from YTMConfig import YTMConfig
from BrowseRequest import BrowseRequest
from BrowseReply import BrowseReply


class YTMClient:
    apiKey: str

    @staticmethod
    def _extractApiKey(input) -> str:
        searchString = "\"INNERTUBE_API_KEY\":\""

        index = input.find(searchString)

        if index < 0:
            return str()

        index += len(searchString)
        endIndex = input.find('"', index)

        if endIndex < 0:
            return str()

        return input[index:endIndex]

    def fetchApiKey(self) -> bool:
        result = requests.get(
            YTMConfig.YTMUSIC_URL,
            headers={"User-Agent": YTMConfig.USER_AGENT}
        )

        self.apiKey = self._extractApiKey(result.text)

        return self.apiKey
    
    def channelInfo(self, channelId):
        reply = BrowseReply(
            BrowseRequest(channelId).send(self.apiKey).text
        )

        return reply

    def channelSongs(self, endpoint):
        songItems = []
        browseId = endpoint.id
        browseParams = endpoint.params

        reply = BrowseReply(
            BrowseRequest(browseId, params=browseParams).send(self.apiKey).text
        )

        playlist = reply.playlists[0]

        songItems += playlist.songs

        while playlist.continuation:
            request = BrowseRequest(browseId, continuation=playlist.continuation)
            response = request.send(self.apiKey)
            reply = BrowseReply(response.text)

            playlist = reply.playlists[0]

            songItems += playlist.songs

        songItems = list(dict.fromkeys(songItems))
        songItems.sort()
        
        return songItems

    def channelAlbums(self, endpoint):
        reply = BrowseReply(
            BrowseRequest(endpoint.id, params=endpoint.params).send(self.apiKey, referer="https://music.youtube.com/channel/UCE5eDJ9T05bGzDvJ5QYsdJQ").text
        )

    def downloadChannel(self, channelId):
        thread = channelId
        def log(string):
            print("[{}] {}".format(thread, string))

        log("Fetching channel info")
        channelInfo = self.channelInfo(channelId)

        thread = channelInfo.header.title
        log("Fetching list of all songs")
        songs = self.channelSongs(channelInfo.allSongsEndpoint)
        log("Got {} songs".format(len(songs)))
        
        time.sleep(1)

        log("Fetching list of all albums")
        albums = self.channelAlbums(channelInfo.allAlbumsEndpoint)
        log("Got {} songs".format(len(songs)))


if __name__ == "__main__":
    client = YTMClient()
    if not client.fetchApiKey():
        print("Could not extract API key!")
        exit(1)

    #client.channelSongs("VLPL1IwCrvt7ePs0Z5aRfjRKfaZt5p_l_yZZ")
    #client.channelInfo("UCE5eDJ9T05bGzDvJ5QYsdJQ")
    client.downloadChannel("UCE5eDJ9T05bGzDvJ5QYsdJQ")
